'use strict';

var semver = require('semver');

var grunt, name, version;

module.exports = {
  getOptions: getOptions
};

function getOptions(args) {
  grunt   = args.grunt;
  name    = args.name;
  version = args.version;

  return {
    clean        : cleanOptions(),
    jshint       : jshintOptions(),
    useminprepare: useminprepareOptions(),
    prompt       : promptOptions(),
  };
}

function cleanOptions() {
  // Empties folders to start fresh
  return {
    dist: {
      options: {
        force: true
      },
      files: [{
        dot: true,
        src: [
          '.tmp',
          '<%= yeoman.dist %>/*',
          '!<%= yeoman.dist %>/.git*',
          '!<%= yeoman.dist %>/Procfile'
        ]
      }]
    },
    server: '.tmp'
  };
}

function jshintOptions() {
  // Make sure code styles are up to par and there are no obvious mistakes
  return {
    options: {
      jshintrc: '<%= yeoman.client %>/.jshintrc',
      reporter: require('jshint-stylish')
    },
    server: {
      options: {
        jshintrc: 'server/.jshintrc'
      },
      src: [ 'server/{,*/}*.js']
    },
    client: {
      options: {
        jshintrc: 'client/.jshintrc'
      },
      src: [ 'client/app/{,*/}*.js', 'client/components/{,*/}*.js', '!client/components/angular-grid/**/*.js']
    },
    all: [
      '<%= yeoman.client %>/{app,components}/**/*.js',
      '!<%= yeoman.client %>/{app,components}/**/*.spec.js',
      '!<%= yeoman.client %>/{app,components}/**/*.mock.js'
    ],
    test: {
      src: [
        '<%= yeoman.client %>/{app,components}/**/*.spec.js',
        '<%= yeoman.client %>/{app,components}/**/*.mock.js'
      ]
    }
  };
}

function promptOptions() {
  return {
    deploy: {
      options: {
        questions: [
          {
            config: 'deploy.prompt.continue',
            type: 'confirm',
            message: 'Are you sure you want to deploy?',
            default: true
          },
        ],
        then: function (results) {
          if (!results['deploy.prompt.continue']) {
            grunt.log.writeln("Exiting...");
            grunt.task.clearQueue();
          }
        }
      }
    },
    editnotes: {
      options: {
        questions: [
          {
            config: 'editnotes.prompt.continue',
            type: 'confirm',
            message: 'Do you want to open your editor ('+process.env.EDITOR+') to type the Release Notes?',
            default: true
          },
        ],
        then: function (results) {
          if (!results['editnotes.prompt.continue']) {
            grunt.log.writeln("Exiting...");
            grunt.task.clearQueue();
          }
        }
      }
    },
    bump: {
      options: {
        questions: [
          {
            config: 'bump.prompt.increment',
            type: 'list',
            message: 'Bump version from ' + version + ' to:',
            choices: [
              {
                value: 'patch',
                name: 'Patch:  ' + semver.inc(version, 'patch') + ' Backwards-compatible bug fixes.'
              },
              {
                value: 'minor',
                name: 'Minor:  ' + semver.inc(version, 'minor') + ' Add functionality in a backwards-compatible manner.'
              },
              {
                value: 'major',
                name: 'Major:  ' + semver.inc(version, 'major') + ' Incompatible API changes.'
              },
              {
                value: 'custom',
                name: 'Custom version number (format: 0.0.0)'
              }
            ]
          },
          {
            config: 'bump.prompt.version',
            type: 'input',
            message: 'What specific version would you like',
            when: function (answers) {
              return answers['bump.prompt.increment'] === 'custom';
            },
            validate: function (value) {
              var valid = semver.valid(value) !== null;
              return valid || 'Must be a valid semver, such as 1.2.3-rc1. See http://semver.org/ for more details.';
            }
          },
          {
            config: 'bump.prompt.useDefaults',
            type: 'confirm',
            message: 'Use default deployment options?',
            default: false
          },
          {
            config: 'bump.options.files',
            type: 'checkbox',
            message: 'What should get the new version:',
            choices: [
              {
                value: 'package.json',
                name: 'package.json' + (!grunt.file.isFile('package.json') ? ' not found, will create one' : ''),
                checked: grunt.file.isFile('package.json')
              },
              {
                value: 'bower.json',
                name: 'bower.json' + (!grunt.file.isFile('bower.json') ? ' not found, will create one' : ''),
                checked: grunt.file.isFile('bower.json')
              }
            ],
            when: function (answers) {
              return answers['bump.prompt.useDefaults'] === false;
            }
          },
          {
              config: 'bump.options.commit',
              type: 'confirm',
              message: 'Should the changes be committed?',
              default: true,
              when: function (answers) {
                return answers['bump.prompt.useDefaults'] === false;
              }
          },
          {
              config: 'bump.options.commitFiles',
              type: 'checkbox',
              message: 'Which files should be commited:',
              when: function (answers) {
                return ( answers['bump.options.commit'] === true && answers['bump.prompt.useDefaults'] === false );
              },
              choices: [
                {
                  value: 'package.json',
                  name: 'package.json' + (!grunt.file.isFile('package.json') ? ' not found' : ''),
                  checked: grunt.file.isFile('package.json')
                },
                {
                  value: 'bower.json',
                  name: 'bower.json' + (!grunt.file.isFile('bower.json') ? ' not found' : ''),
                  checked: grunt.file.isFile('bower.json')
                },
              ]
          },
          {
            config: 'bump.options.createTag',
            type: 'confirm',
            message: 'Create a new tag ?',
            default: true,
            when: function (answers) {
              return ( answers['bump.options.commit'] === true && answers['bump.prompt.useDefaults'] === false );
            }
          },
          {
            config: 'bump.options.push',
            type: 'confirm',
            message: 'Push commited files to repository ?',
            default: true,
            when: function (answers) {
              return ( answers['bump.options.commit'] === true && answers['bump.prompt.useDefaults'] === false );
            }
          }
        ],
        then: function(results) {
          if (!results['bump.options.commit']) {
            grunt.config('bump.options.createTag', false);
            grunt.config('bump.options.push', false);
          }
        }
      }
    }
  };
}
function useminprepareOptions() {
  // Reads HTML for usemin blocks to enable smart builds that automatically
  // concat, minify and revision files. Creates configurations in memory so
  // additional tasks can operate on them.
  return {
    html: ['<%= yeoman.client %>/index.html'],
    options: {
      dest: '<%= yeoman.dist %>/public'
    }
  };
}

