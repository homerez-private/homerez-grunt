'use strict';

var cp = require('child_process');
var fs = require('fs');
var semver = require('semver');

var grunt, name, version;

module.exports = {
  getTasks: getTasks
};

function getTasks(args) {
  grunt   = args.grunt;
  name    = args.name;
  version = args.version;

  return {
    doBump            : doBumpTask,
    doLicenseCheck    : doLicenseCheckTask,
    editReleaseNotes  : editReleaseNotesTask,
    release           : releaseTask,
    verifyMasterBranch: verifyMasterBranchTask,
  };
}

function doBumpTask() {
  if (grunt.config('bump.prompt.increment') === 'custom') {
    grunt.option('setversion', grunt.config('bump.prompt.version'));
    grunt.task.run(['bump']);
  }
  else {
    grunt.task.run(['bump:' + grunt.config('bump.prompt.increment')]);
  }
}
   
function doLicenseCheckTask() {
  var done = this.async();
  var myTerminal = require("child_process").exec,
    commandToBeExecuted = "license-checker --csv  --exclude 'MIT, BSD, ISC, Apache-2.0, Public Domain, CC-BY-3.0, CC0-1.0'";
  myTerminal(commandToBeExecuted, function(error, stdout, stderr) {
    if (!error) {
      grunt.log.writeln(stdout);
      done();
    } else {
      grunt.log.writeln(stderr);
    }
  });
}

function editReleaseNotesTask() {
  var done = this.async();

  var file = (process.env.TMPDIR || process.env.TEMP || '/tmp') + '/notes.txt';

  if (grunt.file.isFile(file)) {
    grunt.log.writeln('Removing old release notes');

    fs.unlinkSync(file);
  }
  var commandArgs = [file];

  if (!grunt.file.isFile(process.env.EDITOR)) {
    grunt.fail.fatal('Could not find your editor. Editor set to ' + process.env.EDITOR);
  }
  grunt.log.writeln('Starting editor');

  var editor = require('child_process').spawn(process.env.EDITOR,  commandArgs, {stdio: 'inherit'});

  editor.on('exit', function (code, signal) {
    if (code !== 0) {
      grunt.fail.fatal('editor exited with code '+ code);
    }

    var newVersion;
    if (grunt.config('bump.prompt.increment') === 'custom') {
      newVersion = grunt.config('bump.prompt.version');
    }
    else {
      newVersion = semver.inc(version, grunt.config('bump.prompt.increment'));
    }

    var header = name + ' ' + newVersion + ' Release Notes\n\n';
    var notes =  grunt.file.read(file);
    if (notes.length === 0 || notes[0] !== '*') {
      grunt.fail.fatal('Invalid release notes format');
    }
    grunt.log.subhead('Setting the Release Notes to');
    grunt.log.writeln(header+notes)

    grunt.config('bump.options.commitMessage', header + notes);

    done();
  });
}

function releaseTask() {
  grunt.task.run([
    'verify-master-branch',
    'prompt:bump',
    'prompt:editnotes',
    'edit-release-notes',
    'do-bump',
  ]);
}

function verifyMasterBranchTask() {
  var done = this.async();
  var onMasterBranch = true;
  
  grunt.log.writeln('Verifying that you\'re on the master branch.');
  var cmd = cp.spawn('git', ['branch']);
  cmd.stdout.on('data', function (data) {
    data.toString().split(/\n/).forEach(function(line) {
      if (line.indexOf('*') === 0 && line !== '* master') {
        onMasterBranch = false;
      }
    });
  });

  cmd.on('close', function (code) {
    if (onMasterBranch) {
      grunt.log.ok('On master branch.');
      done();
    }
    else {
      grunt.fail.fatal('You have to switch to the master branch first.');
    }
  });
}

