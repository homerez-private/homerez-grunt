var Q       = require('q');
var cp      = require('child_process');
var optionConfig = require('./options');
var taskConfig    = require('./tasks');

var grunt, name, version;



module.exports = {
  init: init,

};

function init(args) {
  grunt   = args.grunt;
  name    = args.name;
  version = args.version;

  var tasks = taskConfig.getTasks(args);

  grunt.registerTask('release',              'Make a release',                       tasks.release);
  grunt.registerTask('pack',                 'Make a release',                       tasks.release);
  grunt.registerTask('verify-master-branch', 'Are we on master branch?',             tasks.verifyMasterBranch);
  grunt.registerTask('edit-release-notes',   'Edit Release Notes',                   tasks.editReleaseNotes);
  grunt.registerTask('do-bump',              'Bump version based on prompt results', tasks.doBump);
  grunt.registerTask('license-check',        'Display licenses',                     tasks.doLicenseCheck);

  return {
    git: {
      getCurrentBranch  : gitGetCurrentBranch,
      remoteExists      : gitRemoteExists,
      uncommittedChanges: gitUncommittedChanges,
    },

    heroku: {
      verifyEnvironmentVar: herokuVerifyEnvironmentVar,
    },

    options: optionConfig.getOptions(args),
    tasks: tasks,
  }
}

function gitGetCurrentBranch() {
  var deferred = Q.defer();
  
  var cmd = cp.spawn('git', ['branch']);
  cmd.stdout.on('data', function (data) {
    data.toString().split(/\n/).forEach(function(line) {
      var m = line.match(/\* (.*)$/);
      if (m) {
        deferred.resolve(m[1]);
      }
    });
  });

  return deferred.promise;
}

function gitRemoteExists(branchName) {
  var deferred = Q.defer();
  
  var cmd = cp.spawn('git', ['remote']);
  cmd.stdout.on('data', function (data) {
    var found = false;
    data.toString().split(/\n/).forEach(function(line) {
      if (line && line === branchName) {
        found = true;
      }
    });
    deferred.resolve(found);
  });

  return deferred.promise;
}

function gitUncommittedChanges() {
  var deferred = Q.defer();
  
  var result = [];

  var cmd = cp.spawn('git', ['status', '-s']);

  // If there are no changes, "git status -s" gives no output at all.
  // In that case, cmd.stdout.on('data') is not called at all.
  cmd.stdout.on('data', function (data) {
    result = data.toString().split('\n').filter(function(line) { return line.length > 0 });
  });

  cmd.on('close', function (code) {
    deferred.resolve(result);
  });

  return deferred.promise;
}

function herokuVerifyEnvironmentVar(target, v) {
  var deferred = Q.defer();

  var commandArgs = ['config:get'];
  commandArgs.push('--remote');
  commandArgs.push(target);
  commandArgs.push(v);

  var cmd = cp.spawn('heroku',  commandArgs);
  cmd.stdout.on('data', function (data) {
    if (data.length === 1) {
      deferred.reject(v);
    }
  });

  cmd.stderr.on('data', function (data) {
    grunt.log.error('stderr: ' + data);
  });

  cmd.on('close', function (code) {
    if (code !== 0) {
      grunt.fail.fatal('heroku config:get exited with code ' + code);
      deferred.reject('Error reading config var ' + v);
    }
    deferred.resolve();
  });

  return deferred.promise;
}
