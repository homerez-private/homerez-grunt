# HomeRez grunt module

In your Gruntfile.js:

```
var hg = require('homerez-grunt');
hg.setGrunt(grunt);
```

And now you can use the available functions.